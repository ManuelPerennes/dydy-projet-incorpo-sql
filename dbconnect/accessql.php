

<?php
require 'sql_mdp.php';

try {
    $conn = new PDO("mysql:host=$host;dbname=$dbname", $monuser, $monmdp);
    echo "hosted to $dbname at $host successfully.<br>";
} catch (PDOException $pe) {
    die("Could not host to the database $dbname :" . $pe->getMessage());
}

?>